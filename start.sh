#!/bin/bash
set -e

php -r "readfile('https://getcomposer.org/installer');" | php

php composer.phar install

php app/console doctrine:database:create --connection=default --env=test --no-interaction
php app/console doctrine:schema:create --env=test


php app/console doctrine:database:create --connection=default --no-interaction
php app/console doctrine:schema:create --no-interaction
php app/console doctrine:fixtures:load --no-interaction
php app/cache


phpunit -c app/

php app/console server:run