<?php

namespace ShopBundle\Utils;

class VAT
{
    const STANDARD_VAT = 1.23;
    const REDUCED_VAT = 1.08;

    const STANDARD_VAT_NAME = "standard_vat";
    const REDUCED_VAT_NAME = "reduced_vat";


    /**
     * Get VAT name by value
     *
     * @param $taxValue
     *
     * @return string
     */
    public static function getVatName($taxValue)
    {
        switch ($taxValue) {
            case(self::STANDARD_VAT):
                return self::STANDARD_VAT_NAME;
            case(self::REDUCED_VAT):
                return self::REDUCED_VAT_NAME;
            default:
                return "";
        }
    }
}