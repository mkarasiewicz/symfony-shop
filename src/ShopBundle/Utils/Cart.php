<?php
namespace ShopBundle\Utils;


use Doctrine\ORM\EntityManager;
use ShopBundle\Entity\Item;
use Symfony\Component\HttpFoundation\Session\Session;

class Cart
{

    const CART_SESSION_KEY = "itemsInCart";

    /**
     * @var Session
     */
    private $session;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var CartCalculator
     */
    private $calculator;


    /**
     * @param Session $session
     * @param EntityManager $entityManager
     * @param CartCalculator $calculator
     */
    public function __construct(Session $session, EntityManager $entityManager, CartCalculator $calculator)
    {
        $this->session = $session;
        $this->entityManager = $entityManager;
        $this->calculator = $calculator;
    }

    /**
     * Gets all items grouped by VAT
     */
    public function getItemsGroupedByVat()
    {
        $itemsInCart = [];

        $itemsInCart[VAT::STANDARD_VAT_NAME] = $this->getStandardVatItems();
        $itemsInCart[VAT::REDUCED_VAT_NAME] = $this->getReducedVatItems();

        return $itemsInCart;
    }

    /**
     * Adds item to cart
     *
     * @param Item $item
     */
    public function addItem(Item $item)
    {
        $itemsInCart = $this->session->get(self::CART_SESSION_KEY);
        $itemsInCart[VAT::getVatName($item->getTax())][] = $item->getId();
        $this->session->set(self::CART_SESSION_KEY, $itemsInCart);
    }

    /**
     * Removes item form cart
     *
     * @param Item $item
     */
    public function removeItem(Item $item)
    {
        $itemsInCart = $this->session->get(self::CART_SESSION_KEY);

        $itemInCart = array_search($item->getId(),$itemsInCart[VAT::getVatName($item->getTax())]);

        if($itemInCart!==false){
            unset($itemsInCart[VAT::getVatName($item->getTax())][$itemInCart]);
        }

        $this->session->set(self::CART_SESSION_KEY, $itemsInCart);
    }

    /**
     * Removes all items form cart
     */
    public function removeAllItems()
    {
        $this->session->clear();
    }

    /**
     * Summarize standard vat items
     *
     * @return decimal
     */
    public function totalStandardVatItemsPrice()
    {
        return $this->calculator->sumItems($this->getStandardVatItems());
    }

    /**
     * Summarize reduced vat items
     *
     * @return decimal
     */
    public function totalReducedVatItemsPrice()
    {
        return $this->calculator->sumItems($this->getReducedVatItems());
    }

    /**
     * Returns total price
     *
     * @return decimal
     */
    public function totalPrice()
    {
        $items = array_merge($this->getStandardVatItems(), $this->getReducedVatItems());

        return $this->calculator->sumItems($items);
    }

    private function getStandardVatItems()
    {
        return $this->getItemsFormRepo($this->getItemsIdFormCart(VAT::STANDARD_VAT_NAME));
    }

    private function getReducedVatItems()
    {
        return $this->getItemsFormRepo($this->getItemsIdFormCart(VAT::REDUCED_VAT_NAME));
    }

    private function getItemsIdFormCart($vat)
    {
        $itemsIdFormCartByVat = [];
        if (isset($this->session->get(self::CART_SESSION_KEY)[$vat])) {
            $itemsIdFormCartByVat = $this->session->get(self::CART_SESSION_KEY)[$vat];
        }

        return $itemsIdFormCartByVat;
    }

    private function getItemsFormRepo($itemsID)
    {
        $items = [];

        foreach ($itemsID as $itemID) {
            $items[] = $this->entityManager
                ->getRepository('ShopBundle:Item')
                ->find($itemID);
        }

        return $items;
    }
}