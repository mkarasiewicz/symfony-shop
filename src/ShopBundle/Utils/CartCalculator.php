<?php

namespace ShopBundle\Utils;


class CartCalculator
{

    /**
     * Summarize itmes price
     *
     * @param $items
     *
     * @return int|string
     */
    public function sumItems($items)
    {
        $total = 0;

        foreach($items as $item) {
            $total = bcadd($total, $item->getGrossPrice(), 2);
        }

        return $total;
    }
}