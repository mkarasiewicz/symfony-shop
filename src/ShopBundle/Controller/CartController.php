<?php

namespace ShopBundle\Controller;

use ShopBundle\Entity\Item;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 * Cart controller.
 * @Route("/cart")
 * @package ShopBundle\Controller
 */
class CartController extends Controller
{
    /**
     * Lists all shop items.
     *
     * @Route("/add/item", name="cart_add_item")
     * @Method("POST")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function addAction(Request $request)
    {
        if (!empty($request->get('itemID'))) {
            $em = $this->getDoctrine()->getManager();

            $item = $em->getRepository('ShopBundle:Item')->find($request->get('itemID'));

            $this->get("shop.cart")->addItem($item);
        }

        return $this->redirectToRoute('shop');
    }

    /**
     * Lists all shop items.
     *
     * @Route("/remove/item", name="cart_remove_item")
     * @Method("POST")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeAction(Request $request)
    {
        if (!empty($request->get('itemID'))) {
            $em = $this->getDoctrine()->getManager();

            $item = $em->getRepository('ShopBundle:Item')->find($request->get('itemID'));

            $this->get("shop.cart")->removeItem($item);
        }

        return $this->redirectToRoute('shop');
    }

    /**
     * Lists all shop items.
     *
     * @Route("/remove/all", name="cart_remove_all")
     * @Method("GET")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeAllAction()
    {
        $this->get("shop.cart")->removeAllItems();

        return $this->redirectToRoute('shop');
    }
}
