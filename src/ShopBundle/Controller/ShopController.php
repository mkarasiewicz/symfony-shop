<?php

namespace ShopBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

class ShopController extends Controller
{
    /**
     * Lists all shop items.
     *
     * @Route("/", name="shop")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        return [
            'items' => $em->getRepository('ShopBundle:Item')->findAll(),
            'cart' =>$this->get("shop.cart"),
        ];
    }
}
