<?php

namespace ShopBundle\Tests\Controller;

use Doctrine\ORM\EntityManager;
use ShopBundle\DataFixtures\ORM\LoadItemData;
use ShopBundle\Entity\Item;
use ShopBundle\Utils\Cart;
use ShopBundle\Utils\VAT;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\Tools\SchemaTool;

class CartControllerTest extends WebTestCase
{

    /**
     * @var Client
     */
    private $client;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @before
     */
    public function setUp()
    {
        $this->client = static::createClient();
        $this->entityManager = $this->client->getContainer()->get('doctrine')->getManager();
    }

    /**
     * @after
     */
    public function tearDown()
    {
        $this->entityManager->close();
    }


    /**
     * @test
     */
    public function shouldRedirectToIndex_WhenAddActionCalled()
    {
        //given

        //when
        $this->client->request('POST', '/cart/add/item');

        //then
        $this->assertEquals(Response::HTTP_FOUND, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @test
     */
    public function shouldAddItemToCart_WhenItemIDPassedToAddAction()
    {
        //given
        $itemID = 1;
        $this->loadItems($itemID);

        //when
        $this->client->request('POST', '/cart/add/item', ['itemID' => $itemID]);

        //then
        $items = $this->getItemsFormCart();
        $this->assertEquals(Response::HTTP_FOUND, $this->client->getResponse()->getStatusCode());
        $this->assertLessThanOrEqual(1, count($items[VAT::REDUCED_VAT_NAME]));
        $this->assertLessThanOrEqual(1, count($items[VAT::STANDARD_VAT_NAME]));

    }

    /**
     * @test
     */
    public function shouldRedirectToIndex_WhenRemoveActionCalled()
    {
        //given

        //when
        $this->client->request('POST', '/cart/remove/item');

        //then
        $this->assertEquals(Response::HTTP_FOUND, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @test
     */
    public function shouldRemoveItemFromCart_WhenItemIDPassedToRemoveAction()
    {
        //given
        $itemID = 1;
        $this->loadItems($itemID);

        //when
        $this->client->request('POST', '/cart/remove/item', ['itemID' => $itemID]);

        //then
        $items = $this->getItemsFormCart();
        $this->assertCount(0, $items[VAT::REDUCED_VAT_NAME]);
        $this->assertCount(0, $items[VAT::STANDARD_VAT_NAME]);

    }

    /**
     * @test
     */
    public function shouldRedirectToIndex_WhenRemoveAllActionCalled()
    {
        //given

        //when
        $this->client->request('GET', '/cart/remove/all');

        //then
        $this->assertEquals(Response::HTTP_FOUND, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @test
     */
    public function shouldRemoveAllItemsFromCart_WhenRemoveActionCalled()
    {
        //given
        $this->loadItems(5);

        //when
        $this->client->request('GET', '/cart/remove/all');

        //then
        $items = $this->getItemsFormCart();
        $this->assertCount(0, $items[VAT::REDUCED_VAT_NAME]);
        $this->assertCount(0, $items[VAT::STANDARD_VAT_NAME]);

    }

    private function loadItems($numberOfItems)
    {
        $fixtures = new LoadItemData($numberOfItems);
        $fixtures->load($this->entityManager);
    }

    private function getItemsFormCart()
    {
        $cart = $this->client->getContainer()->get('shop.cart');

        return $cart->getItemsGroupedByVat();
    }
}
