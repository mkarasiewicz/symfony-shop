<?php

namespace ShopBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Component\HttpFoundation\Response;

class ShopControllerTest extends WebTestCase
{

    /**
     * @var Client
     */
    private $client;

    /**
     * @before
     */
    public function setUp()
    {
        $this->client = static::createClient();
    }


    /**
     * @test
     */
    public function shouldResponseIndexAction()
    {
        //given

        //when
        $this->client->request('GET', '/');

        //then
        $this->assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @test
     */
    public function shouldRenderItemsList()
    {
        //given

        //when
        $crawler = $this->client->request('GET', '/');

        //then
        $this->assertEquals(1, $crawler->filter('div.items-list')->count(), 'Missing summary element');
    }

    /**
     * @test
     */
    public function shouldRenderCartItemsList()
    {
        //given

        //when
        $crawler = $this->client->request('GET', '/');

        //then
        $this->assertEquals(1, $crawler->filter('div.cart-items-list')->count(), 'Missing summary element');
    }

    /**
     * @test
     */
    public function shouldRenderCartSummary()
    {
        //given

        //when
        $crawler = $this->client->request('GET', '/');

        //then
        $this->assertEquals(1, $crawler->filter('div.summary')->count(), 'Missing summary element');
    }
}
