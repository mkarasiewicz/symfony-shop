<?php

namespace ShopBundle\Tests\Utils;


use ShopBundle\Utils\Cart;
use ShopBundle\Utils\VAT;

class CartTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Cart
     */
    private $cart;

    private $session;

    private $itemRepository;

    private $entityManager;

    private $cartCalculator;

    /**
     * @before
     */
    public function setUp()
    {
        $this->session = $this->getSessionMock();
        $this->entityManager = $this->getEntityManagerMock();
        $this->cartCalculator = $this->getCartCalculatorMock();

        $this->cart = new Cart($this->session, $this->entityManager, $this->cartCalculator);
    }

    /**
     * @test
     */
    public function shouldReturnEmptyArray_WhenNoItemsAddedToCart()
    {
        //given
        $this->sessionShouldHaveNoItems();

        //when
        $items = $this->cart->getItemsGroupedByVat();

        //then
        $emptyItemsArray = [
            VAT::STANDARD_VAT_NAME => [],
            VAT::REDUCED_VAT_NAME => []
        ];
        $this->assertArraySubset($emptyItemsArray, $items);
    }

    /**
     * @test
     */
    public function shouldReturnItemsArrayGroupedByVat_WhenItemInCartExist()
    {
        //given
        $itemId = 1;
        $item = $this->getItemMock($itemId, VAT::STANDARD_VAT);
        $this->sessionShouldHaveItemsId($itemId, VAT::STANDARD_VAT_NAME);

        //when
        $items = $this->cart->getItemsGroupedByVat();

        //then
        $this->assertCount(1, $items[VAT::STANDARD_VAT_NAME]);
        $this->assertSame($items[VAT::STANDARD_VAT_NAME][0], $item);
    }

    /**
     * @test
     */
    public function shouldAddItemToCart_WhenItemPassed()
    {
        //given
        $itemId = 1;
        $item = $this->getItemMock($itemId, VAT::STANDARD_VAT);
        $this->sessionShouldHaveNoItems();

        //then
        $items[VAT::STANDARD_VAT_NAME][] = $itemId;
        $this->session
            ->shouldReceive('set')
            ->with(Cart::CART_SESSION_KEY, $items)
            ->once();

        //when
        $this->cart->addItem($item);

    }

    /**
     * @test
     */
    public function shouldRemoveItemFromCart_WhenItemPassed()
    {
        //given
        $itemId = 1;
        $item = $this->getItemMock($itemId, VAT::STANDARD_VAT);
        $this->sessionShouldHaveItemsId($itemId, VAT::STANDARD_VAT_NAME);

        //then
        $this->session
            ->shouldReceive('set')
            ->with(Cart::CART_SESSION_KEY, [VAT::STANDARD_VAT_NAME => []])
            ->once();

        //when
        $this->cart->removeItem($item);
    }

    /**
     * @test
     */
    public function shouldRemoveAllItemsFromCart()
    {
        //given
        $this->session
            ->shouldReceive('clear')
            ->once();

        //when
        $this->cart->removeAllItems();
    }

    /**
     * @test
     */
    public function shouldReturnSummarizedStandardVatItemPrice()
    {
        //given
        $itemOneId = 1;
        $itemTwoId = 2;
        $itemOne = $this->getItemMock($itemOneId, VAT::STANDARD_VAT);
        $itemTwo = $this->getItemMock($itemTwoId, VAT::STANDARD_VAT);

        $this->sessionShouldHaveItemsIds([$itemOneId, $itemTwoId], VAT::STANDARD_VAT_NAME);

        //then
        $this->cartCalculator
            ->shouldReceive('sumItems')
            ->with([$itemOne, $itemTwo])
            ->once();

        //when
        $this->cart->totalStandardVatItemsPrice();

    }

    /**
     * @test
     */
    public function shouldReturnSummarizedReducedVatItemPrice()
    {
        //given
        $itemOneId = 1;
        $itemTwoId = 2;
        $itemOne = $this->getItemMock($itemOneId, VAT::REDUCED_VAT);
        $itemTwo = $this->getItemMock($itemTwoId, VAT::REDUCED_VAT);

        $this->sessionShouldHaveItemsIds([$itemOneId, $itemTwoId], VAT::REDUCED_VAT_NAME);

        //then
        $this->cartCalculator
            ->shouldReceive('sumItems')
            ->with([$itemOne, $itemTwo])
            ->once();

        //when
        $this->cart->totalReducedVatItemsPrice();
    }

    /**
     * @test
     */
    public function shouldReturnTotalPriceOfCart()
    {
        //given
        $itemStandardVatId = 1;
        $itemStandardVat = $this->getItemMock($itemStandardVatId, VAT::STANDARD_VAT);

        $itemReducedVatId = 2;
        $itemReducedVat = $this->getItemMock($itemReducedVatId, VAT::REDUCED_VAT);

        $this->sessionShouldHaveItemsGroupedByVat(
            [
                VAT::STANDARD_VAT_NAME => [$itemStandardVatId],
                VAT::REDUCED_VAT_NAME => [$itemReducedVatId]
            ]
        );

        //then
        $this->cartCalculator
            ->shouldReceive('sumItems')
            ->with([$itemStandardVat, $itemReducedVat])
            ->once();

        //when
        $this->cart->totalPrice();
    }


    private function getItemMock($id, $vat)
    {
        $item = \Mockery::mock('ShopBundle\Entity\Item')
            ->shouldReceive('getId')
            ->andReturn($id)
            ->shouldReceive('getTax')
            ->andReturn($vat)
            ->mock();

        $this->itemRepository
            ->shouldReceive('find')
            ->with($id)
            ->andReturn($item)
            ->mock();

        return $item;
    }

    private function sessionShouldHaveItemsId($itemsId, $vat)
    {
        $this->sessionShouldHaveItemsIds([$itemsId], $vat);
    }

    private function sessionShouldHaveItemsIds($itemsId, $vat)
    {
        $this->sessionShouldHaveItemsGroupedByVat([$vat => $itemsId]);
    }

    private function sessionShouldHaveItemsGroupedByVat($itemsGroupedByVat)
    {
        $this->session
            ->shouldReceive('get')
            ->with(Cart::CART_SESSION_KEY)
            ->andReturn($itemsGroupedByVat);
    }

    private function sessionShouldHaveNoItems()
    {
        $this->session
            ->shouldReceive('get')
            ->with(Cart::CART_SESSION_KEY)
            ->andReturn([]);

    }

    private function getSessionMock()
    {
        return \Mockery::mock('Symfony\Component\HttpFoundation\Session\Session');
    }

    private function getCartCalculatorMock()
    {
        return \Mockery::mock('ShopBundle\Utils\CartCalculator');
    }

    private function getEntityManagerMock()
    {
        $this->itemRepository = $this->getItemRepositoryMock();

        return \Mockery::mock('Doctrine\ORM\EntityManager')
            ->shouldReceive('getRepository')
            ->with('ShopBundle:Item')
            ->andReturn($this->itemRepository)
            ->mock();
    }

    private function getItemRepositoryMock()
    {
        return \Mockery::mock('Doctrine\ORM\EntityRepositoryDoctrine\ORM\EntityRepository');
    }
}