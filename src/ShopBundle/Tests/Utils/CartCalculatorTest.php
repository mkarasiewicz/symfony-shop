<?php

namespace ShopBundle\Tests\Utils;


use ShopBundle\Utils\CartCalculator;

class CartCalculatorTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var CartCalculator
     */
    private $cartCalculator;

    /**
     * @before
     */
    public function setUp()
    {
        $this->cartCalculator = new CartCalculator();
    }

    /**
     * @test
     */
    public function shouldReturn0_WhenNoItemsPassed()
    {
        //given
        $items = [];
        //then
        $total = $this->cartCalculator->sumItems($items);
        //when
        $this->assertEquals(0, $total);
    }

    /**
     * @test
     */
    public function shouldReturnTotalPrice_WhenTwoItemsPassed()
    {
        //given
        $itemOneGrossPrice = 10.23;
        $itemTwoGrossPrice = 20.23;
        $totalPrice = bcadd($itemOneGrossPrice, $itemTwoGrossPrice, 2);

        //when
        $items = [$this->getItemMock($itemOneGrossPrice), $this->getItemMock($itemTwoGrossPrice)];
        //then
        $this->assertEquals($totalPrice, $this->cartCalculator->sumItems($items));
    }

    /**
     * @test
     */
    public function shouldReturnZero_WhenPassedWithInvalidData()
    {
        //given
        $itemOneGrossPrice = "item1";
        $itemTwoGrossPrice = "item2";

        //when
        $items = [$this->getItemMock($itemOneGrossPrice), $this->getItemMock($itemTwoGrossPrice)];
        //then
        $this->assertEquals(0, $this->cartCalculator->sumItems($items));
    }

    private function getItemMock($grossPrice) {
        return \Mockery::mock('ShopBundle\Entity\Item')
            ->shouldReceive('getGrossPrice')
            ->andReturn($grossPrice)
            ->mock();
    }
}