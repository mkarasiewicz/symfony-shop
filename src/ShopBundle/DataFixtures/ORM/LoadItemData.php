<?php

namespace ShopBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use ShopBundle\Entity\Item;
use ShopBundle\Utils\Category;
use ShopBundle\Utils\VAT;

class LoadItemData implements FixtureInterface
{
    private $numberOfItemsToLoad;

    /**
     * Constructor
     *
     * @param $numberOfItemsToLoad
     */
    public function __construct($numberOfItemsToLoad = 9) {
        $this->numberOfItemsToLoad = $numberOfItemsToLoad;
    }

    public function load(ObjectManager $manager)
    {
        $generator = \Faker\Factory::create();
        $populator = new \Faker\ORM\Doctrine\Populator($generator, $manager);

        $populator->addEntity('ShopBundle\Entity\Item', $this->numberOfItemsToLoad,
            [
                'name' => function() use ($generator) {return $generator->word;},
                'quantity' => function() use ($generator) {return $generator->numberBetween(1,100);},
                'category' => function() use ($generator) {return $generator->randomElement([Category::FOOD, Category::CATS]);},
                'tax' => function() use ($generator) {return $generator->randomElement([VAT::STANDARD_VAT, VAT::REDUCED_VAT]);}
            ]
        );

        $populator->execute();
    }
}